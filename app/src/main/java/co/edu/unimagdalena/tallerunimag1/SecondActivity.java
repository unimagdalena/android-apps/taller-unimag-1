package co.edu.unimagdalena.tallerunimag1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class SecondActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private TextInputLayout data1, data2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        data1 = findViewById((R.id.data1_input));
        data2 = findViewById((R.id.data2_input));
        radioGroup = (RadioGroup) findViewById(R.id.operations);

        final Button resultButton = findViewById(R.id.result_button);
        resultButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Code here executes on main thread after user presses button
                if (view.getId() == R.id.result_button) {
                    // get selected radio button from radioGroup
                    int selectedId = radioGroup.getCheckedRadioButtonId();
                    // find the radiobutton by returned id
                    //radioButton = (RadioButton) findViewById(selectedId);

                    // validate data
                    String data1value = data1.getEditText().getText().toString();
                    String data2value = data2.getEditText().getText().toString();
                    if (TextUtils.isEmpty(data1value) || TextUtils.isEmpty(data2value)) {
                        Toast.makeText(getApplicationContext(),"Datos inválidos", Toast.LENGTH_LONG).show();
                        return;
                    }

                    Integer value1 = Integer.parseInt(data1value);
                    Integer value2 = Integer.parseInt(data2value);

                    switch (selectedId) {
                        case R.id.plus:
                            Toast.makeText(getApplicationContext(), "Suma: "+(value1 + value2), Toast.LENGTH_LONG).show();
                            break;
                        case R.id.minus:
                            Toast.makeText(getApplicationContext(), "Resta: "+(value1 - value2), Toast.LENGTH_LONG).show();
                            break;
                        case R.id.mul:
                            Toast.makeText(getApplicationContext(), "Multiplicación: "+(value1 * value2), Toast.LENGTH_LONG).show();
                            break;
                        case R.id.div:
                            if (value2 != 0)
                                Toast.makeText(getApplicationContext(), "División: "+(value1 / value2), Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(getApplicationContext(), "No se puede vividir entre "+value2, Toast.LENGTH_LONG).show();
                            break;
                        case R.id.mcd:
                            Toast.makeText(getApplicationContext(), "MCD: "+(mcd(value1, value2)), Toast.LENGTH_LONG).show();
                            break;
                        case R.id.mcm:
                            Toast.makeText(getApplicationContext(), "MCM: "+(mcm(value1, value2)), Toast.LENGTH_LONG).show();
                            break;
                        case R.id.mayor:
                            Toast.makeText(getApplicationContext(), "Mayor: "+Integer.max(value1, value2), Toast.LENGTH_LONG).show();
                            break;
                        case R.id.menor:
                            Toast.makeText(getApplicationContext(), "Menor: "+Integer.min(value1, value2), Toast.LENGTH_LONG).show();
                            break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + radioButton.getText().toString());
                    }
                }
            }
        });
    }

    public Integer mcd(Integer num1, Integer num2) {
        while(!num1.equals(num2)){
            if(num1>num2){
                num1= num1-num2;
            }else{
                num2= num2 -num1;
            }
        }
        return num1;
    }

    public Integer mcm(Integer num1, Integer num2) {
        int i = 2, mcm = 1;
        while(i <= num1 || i <= num2) {
            if(num1%i==0 || num2%i==0) {
                mcm=mcm*i;
                if(num1%i==0) num1=num1/i;
                if(num2%i==0) num2=num2/i;
            }
            else
                i=i+1;
        }
        return mcm;
    }
}