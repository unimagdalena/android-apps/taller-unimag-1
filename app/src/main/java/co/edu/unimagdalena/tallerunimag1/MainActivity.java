package co.edu.unimagdalena.tallerunimag1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout pass, user;
    private Switch remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = findViewById(R.id.data1_input);
        pass = findViewById(R.id.data2_input);
        remember = findViewById(R.id.remember);

        final Button loginButton = findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                String userValue = user.getEditText().getText().toString();
                String passValue = pass.getEditText().getText().toString();
                if(TextUtils.isEmpty(userValue) || TextUtils.isEmpty(passValue)){
                    Toast.makeText(getApplicationContext(),"Datos inválidos", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}